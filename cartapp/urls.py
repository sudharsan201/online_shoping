from django.urls import path

from cartapp import views


urlpatterns = [
    path("api/product/",views.Product_List.as_view()),
    path("api/products/<int:pk>",views.ProductDetails.as_view()),
    path("api/order/",views.Order_List.as_view()),
    path("api/orders/<int:pk>",views.OrderDetails.as_view()),
    path("api/orderitem/",views.OrderItem_List.as_view()),
    path("api/orderitems/<int:pk>",views.OrderItemDetails.as_view()),
    path("search/",views.OrderItemDetails.as_view()),

]