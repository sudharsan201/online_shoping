from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class Product(models.Model):
    Title = models.CharField(max_length=30)
    Description = models.CharField(max_length=150)
    ImageLink = models.FileField(upload_to='photos/', blank=True, null=True)
    Price = models.FloatField()
    Created_At = models.DateField(auto_now_add=True, null=True)
    Updated_At = models.DateField(auto_now=True, null=True)

    def _str_(self):
        return f"{self.id}{self.Title}"


class Order(models.Model):
    UserId = models.ForeignKey(User, on_delete=models.CASCADE)
    Total = models.IntegerField(default=0)
    Created_At = models.DateField(auto_now_add=True, null=True)
    Updated_At = models.DateField(auto_now=True, null=True)
    STATUS = (
        (1, ('New')),
        (2, ('Paid')),

    )
    Status = models.PositiveSmallIntegerField(
        choices=STATUS,
        default=1,
    )
    PAYMENT = (
        (1, ('Cash_OnDelivery')),
        (2, ('Paytm')),
        (3, ('Card')),

    )
    PaymentModel = models.PositiveSmallIntegerField(
        choices=PAYMENT,
        default=1,
    )

    def _str_(self):
        return f'{self.id}-{self.UserId}-{self.Total}-{self.Created_At}{self.Updated_At}{self.Status}{self.PaymentModel}'


class OrderItem(models.Model):
    OrderId = models.ForeignKey(Order, on_delete=models.CASCADE)
    ProductId = models.ForeignKey(Product, on_delete=models.CASCADE)
    Quantity=models.IntegerField(default=1)
    Price=models.FloatField(default=0)


    def _str_(self):
        return f'{self.id}{self.OrderId}{self.ProductId}{self.Quantity}{self.Price}'
