from rest_framework import permissions, status, generics, filters
from rest_framework.authentication import BasicAuthentication
from rest_framework.decorators import api_view
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.filters import SearchFilter, OrderingFilter

from .models import Product, Order, OrderItem
from .serializers import  ProductSerializer, OrderSerializer,OrderItemSerializer

class ProductPagination(PageNumberPagination):
    Page_size = 3




class Product_List(generics.ListCreateAPIView):
    def get_queryset(self):
        return Product.objects.all()

    serializer_class = ProductSerializer
    search_fields = ['id','Title']
    filter_backends = (SearchFilter,OrderingFilter)
    Pagination_class = ProductPagination

    



class ProductDetails(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return  Product.objects.all()
    serializer_class =  ProductSerializer

class Order_List(generics.ListCreateAPIView):
    def get_queryset(self):
        return Order.objects.all()

    serializer_class = OrderSerializer
    authentication_classes = [BasicAuthentication]


class OrderDetails(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return  Order.objects.all()
    serializer_class =  OrderSerializer

class OrderItem_List(generics.ListCreateAPIView):
    def get_queryset(self):
        return OrderItem.objects.all()

    serializer_class = OrderItemSerializer
    authentication_classes = [BasicAuthentication]

class OrderItemDetails(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return  OrderItem.objects.all()
    serializer_class =  OrderItemSerializer

#
# class products_search(generics.ListAPIView):
#     search_fields=['Title']
#     filter_backends = (filters.SearchFilter,)
#     queryset = Product.objects.all()
#     serializer_class = ProductSerializer

